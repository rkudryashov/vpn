#!/bin/bash

MyServerIP="172.16.10.1/32"
MyNetwork="172.16.10.0/24"

#remove all previous made rules:
sudo iptables --flush

#Allow access to Loopback interfaces:
sudo iptables -A INPUT -i lo -j ACCEPT

#Allow any already established connections:
sudo iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT


#Allow access from MyNetwork via ssh:
sudo iptables -A INPUT -s 0.0.0.0/0 -d 0.0.0.0/0 -p tcp --dport 22 -j ACCEPT

#Allow accsess from MyNetwork via sip:
sudo iptables -A INPUT -s $MyNetwork -d $MyServerIP -p udp --dport 100:7776 -j ACCEPT
sudo iptables -A INPUT -s $MyNetwork -d $MyServerIP -p udp --dport 7778:100000 -j ACCEPT
#sudo iptables -A INPUT -s $MyNetwork -d $MyServerIP -p udp --dport 4569 -j ACCEPT
#sudo iptables -A INPUT -s $MyNetwork -d $MyServerIP -p udp --dport 2427 -j ACCEPT

sudo iptables -t nat -A POSTROUTING -s 172.16.10.0/24 -o eth0 -j MASQUERADE
sudo iptables -A INPUT -s 0.0.0.0/0 -d 0.0.0.0/0 -p udp --dport 7777 -j ACCEPT
#Allow access with ICMP:
sudo iptables -A INPUT -p icmp -j ACCEPT

#Drop all other connections:
sudo iptables -A INPUT -j DROP

sudo iptables -L -n -v --line-numbers
